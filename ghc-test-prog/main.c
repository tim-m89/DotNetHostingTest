#include <stdio.h>
#include <windows.h>

typedef BOOL (__cdecl *startDotNetHostType)(); 

int main(int argc, char *argv[])
{

  HMODULE hinstLib = LoadLibrary("DotNetHostingLib.dll");

  startDotNetHostType ptr = (startDotNetHostType)GetProcAddress(hinstLib, "startDotNetHost"); 

  ptr();

  return 0;
}
