
#include "DotNetHostingLib.h"
#include <metahost.h>
#pragma comment(lib, "mscoree.lib")
#import "mscorlib.tlb" raw_interfaces_only				\
    high_property_prefixes("_get","_put","_putref")		\
    rename("ReportEvent", "InteropServices_ReportEvent")
using namespace mscorlib;


HRESULT hr;

ICLRMetaHost    *pMetaHost = NULL;
ICLRRuntimeInfo *pRuntimeInfo = NULL;
ICLRRuntimeHost *pClrRuntimeHost = NULL;
ICorRuntimeHost *pCorRuntimeHost = NULL;

extern "C" {

	__declspec(dllexport) BOOL startDotNetHost()
	{
		wprintf(L"1\n");

		HMODULE hMscoree = LoadLibrary("mscoree");

		wprintf(L"2\n");

		CLRCreateInstanceFnPtr CLRCreateInstanceFn = (CLRCreateInstanceFnPtr)GetProcAddress(hMscoree, "CLRCreateInstance");

		wprintf(L"3\n");

		hr = CLRCreateInstanceFn(CLSID_CLRMetaHost, IID_PPV_ARGS(&pMetaHost));
		if (FAILED(hr))
		{
			wprintf(L"CLRCreateInstance failed w/hr 0x%08lx\n", hr);
			return FALSE;
		}

		wprintf(L"4\n");

		hr = pMetaHost->GetRuntime(L"v4.0.30319", IID_PPV_ARGS(&pRuntimeInfo));
		if (FAILED(hr))
		{
			wprintf(L"ICLRMetaHost GetRuntime failed w/hr 0x%08lx\n", hr);
			return FALSE;
		}

		wprintf(L"5\n");

		hr = pRuntimeInfo->GetInterface(CLSID_CLRRuntimeHost, IID_PPV_ARGS(&pClrRuntimeHost));
		if (FAILED(hr))
		{
			wprintf(L"ICLRRuntimeInfo GetInterface CLRRuntimeHost failed w/hr 0x%08lx\n", hr);
			return FALSE;
		}

		wprintf(L"6\n");

		hr = pClrRuntimeHost->Start();
		if (FAILED(hr))
		{
			wprintf(L"ICLRRuntimeHost Start failed w/hr 0x%08lx\n", hr);
			return FALSE;
		}

		wprintf(L"7\n");

		hr = pRuntimeInfo->GetInterface(CLSID_CorRuntimeHost, IID_PPV_ARGS(&pCorRuntimeHost));
		if (FAILED(hr))
		{
			wprintf(L"ICLRRuntimeInfo GetInterface CorRuntimeHost failed w/hr 0x%08lx\n", hr);
			return FALSE;
		}

		wprintf(L"8\n");

		return TRUE;
	}

	void stopDotDotHost()
	{
		if (pCorRuntimeHost)
			pCorRuntimeHost->Release();

		if (pClrRuntimeHost)
		{
			hr = pClrRuntimeHost->Stop();
			if (FAILED(hr))
				wprintf(L"ICLRRuntimeHost Stop failed w/hr 0x%08lx\n", hr);
			pClrRuntimeHost->Release();
		}

		if (pRuntimeInfo)
			pRuntimeInfo->Release();

		if (pMetaHost)
			pMetaHost->Release();
	}


	ICorRuntimeHost * getCorRuntimeHost()
	{

		return pCorRuntimeHost;
	}

}
