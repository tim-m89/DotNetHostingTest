#pragma once

#include <windows.h>

#ifdef DLLExport
	#define DLLAPI __declspec(dllexport)
#else
	#define DLLAPI __declspec(dllimport)
#endif


#ifdef __cplusplus
	extern "C" {
#endif

	DLLAPI BOOL startDotNetHost();

#ifdef __cplusplus
	}
#endif